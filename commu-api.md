# Commu API

Hello there. Thanks for using Commu API. This documentation try to cover all Commu API.

## Structure

The Envelope :

```json
{
  "meta": {
    "code": 200,
    "message": "API call successful.",
  },
  "data": ...
}
```
Error Response : 

```json
{
    "meta": {
        "code": 500,
		"status": "Error",
    },
    "data" : ...
}
```


### The Envelope
Every response is contained by an envelope. That is, each response has a predictable set of keys with which you can expect to interact:

### META
The meta key is used to communicate extra information about the response to the developer. If all goes well, you'll only ever see a code key with value 200. However, sometimes things go wrong, and in that case you might see a response like:

### DATA

The data key is the meat of the response. It may be a list or dictionary, but either way this is where you'll find the data you requested.

<aside class="notice">With the exception is OAuth2 API, that will explained later in Authorize section.</aside>

## Datatype
```json
{
  "meta": {
    "code": 200,
    "message": "Success."
  },
  "data": {
    "id": "5639ce5e778b5fa40148bed3",
    "number": 2,
    "username": "smittyfirst",
    "name": "Smitty Werben Jager man Jensen",
    "alive": false,
    "date": "2015-11-19T09:47:46.493Z"
  }
}
```
* Id or such will always be a string, even if it's look like a number. 

* Response with number will return number (ex. `1`, `322`), not quoted number.

* Respectively, bool will return `true` or `false`.

* Date will be returned as ISO 8601 string. Example `December 25th, 2015` will be sent as `"2015-12-25T00:00:00Z"`.


## Header

Use `appication/json` as `Content-Type` header. Even without it this API would not break, it's still a good practice.

Do note every API need you to provide either `access_token` or Authorization Header. Use following id and secret to generate correct header.

|Basic Auth|Public|
|-|-|
|ID|dycode|
|Secret|dycode|

|Bearer Token|User Access Token|
|-|-|
|Value|{{ refers to `access_token` in Login API ex : `c29504439314c5b072c665db8f2e9e650cc199ea` }}|



## Authentication

### Register

Request : 


* Link : POST {{ domain }}/api/v1/register

* Authorization : Basic Auth


|Field|Required|Value|
|-|-|-|
|fullname|yes|string|
|email|yes|string|
|username|yes|string|
|password|yes|string|
|passwordConfirmation|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Successful Registered"
	},
	"data": "Link Email Verification has been sent to your Email, please check them"
}
```

Error Response : 

```
json
{
	"meta": {
		"code": 400,
		"message": "Email Already"
	},
	"data": "Email is already exist"
}
```



### Login
Request : 


* Link : POST {{ domain }}/api/v1/signin

* Authorization : Basic Auth

|Field|Required|Value|
|-|-|-|
|grant_type|yes|password|
|username|yes|string refers to `username` or `email` example : `viercas` or `farhan.naufalghani@gmail.com`|
|password|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Token Generated"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
			"__v": 0
		},
		"access_token": "0b0f2d4393a582d994650a13faad98ac2236ae91",
		"refresh_token": "5d06a20c02f2e154d62782470d23bf872ae39ac7",
		"accessTokenExpiresAt": "2017-09-20T05:26:51.401Z",
		"refreshTokenExpiresAt": "2017-10-04T04:26:51.402Z"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Email or Username Invalid"
	},
	"data": "Email or Username Not Found or Invalid"
}
```

### Refresh Login
Request : 

* Description : If you got Token expired you can got new Token by serving `refresh_token`

* Link : POST {{ domain }}/api/v1/signin

* Authorization : Basic Auth

|Field|Required|Value|
|-|-|-|
|grant_type|yes|refresh_token|
|refresh_token|yes|string refers to `refresh_token` in Login module ex : `e7e1d4e8690f4d9dd8722e4a8e907664d49c3069`|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Token Generated"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
			"__v": 0
		},
		"access_token": "d729f267b458ec40a49cdf5b45a9c2a78308f32d",
		"refresh_token": "0c11c8dcf14afc30192d35efc59928db17ceb2c7",
		"accessTokenExpiresAt": "2017-09-28T11:55:25.575Z",
		"refreshTokenExpiresAt": "2017-10-12T10:55:25.575Z"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "Refresh Token Invalid"
}
```
### Forgot Password

Request : 


* Link : POST {{ domain }}/api/v1/forgot

* Authorization : Basic Auth



|Field|Required|Value|
|-|-|-|
|email|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "SUCCESS"
	},
	"data": "Link forgot Password has been sent to Your Email"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "Email not Found"
}
```

## User

### Profile
Request : 


* Link : GET {{ domain }}/api/v1/user

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Data found"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
            "id_user": "59f1b90edc29de41d8ceaf2a"
		},
		"userdata": {
			"photo": "http://localhost:4000/img/59c1ed18aa0e99336863ec99.jpg",
			"fullname": "Farhan Naufal Ghani",
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Update Profile

Request : 


* Link : POST {{ domain }}/api/v1/user

* Authorization : Bearer Token

* Header : enctype = multipart/form-data


|Field|Required|Value|
|-|-|-|
|username|no|string|
|fullname|no|string|
|photo|no|file|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Profile Updated"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
			"__v": 0
		},
		"userdata": {
			"photo": "http://localhost:4000/img/59c1ed18aa0e99336863ec99.jpg",
			"fullname": "Farhan Naufal Ghani",
			"__v": 0,
			"friends": []
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Email or Username Invalid"
	},
	"data": "Email or Username Not Found or Invalid"
}
```

### Change Password
Request : 


* Link : POST {{ domain }}/api/v1/changePassword

* Authorization : Bearer Token



|Field|Required|Value|
|-|-|-|
|oldPassword|yes|string|
|newPassword|yes|string|
|newPasswordConfirmation|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Password has Changed"
	},
	"data": {
		"user": {
			"username": "farhan.naufalghani@gmail.com",
			"__v": 0
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Password not Match"
	},
	"data": "New Password and Retype New Password not Match"
}
```

### Logout
Request : 


* Link : GET {{ domain }}/api/v1/logout

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "Success Logout"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Get User By Username

Request : 


* Link : GET {{ domain }}/api/v1/user/find/{{ refers to `username` example: `viercas` }}

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Data found"
	},
	"data": {
		"user": {
			"username": "themen",
			"email": "themenkolk@gmail.com",
			"__v": 0
		},
		"userdata": {
			"__v": 1,
			"photo": null,
			"fullname": null
		},
		"friend": true
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "User Not Found"
}
```


### Search User By Username
Request : 

* Description : This is sser search engine who not showing Friends

* Link : GET {{ domain }}/api/v1/user/search/{{ refers to any kind of `username` example: `vi` }}?offset={{ refers to `offset` example : `1` or `2` }}&limit={{ refers to `limit` example : `1` or `2` }}

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"username": "themen",
			"email": "themenkolk@gmail.com",
			"data": {
				"fullname": null,
				"photo": null
			}
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

## Friend

### My Friends

Request : 


* Link : GET {{ domain }}/api/v1/user/friend?search={{ refers to `username` example : `e` if this null, showing much user }}&offset={{ refers to `offset` example : `1` or `2` }}&limit={{ refers to `limit` example : `1` or `2` }}

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"username": "vier",
			"email": "farhan.asd@gmail.com",
			"data": {
				"fullname": null,
				"photo": null
			}
		},
		{
			"username": "themen",
			"email": "themenkolk@gmail.com",
			"data": {
				"fullname": null,
				"photo": null
			}
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Add Friend

Request : 


* Link : POST {{ domain }}/api/v1/user/friend

* Authorization : Bearer Token

|Field|Required|Value|
|-|-|-|
|user|yes|string refers to `username` example : `themen` |

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Friend themen Has Been Added !"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "Friend has been Added !"
}
```

## Room

### My Room

Request : 

* Link : GET {{ domain }}/api/v1/room

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"id_room": "room_59f1ba17a8c0263d68187169",
			"name": null,
			"type": "private",
			"photo": null,
			"date": "2017-10-26T10:33:54.711Z",
			"user": [
				{
					"id_user": "59f1b90edc29de41d8ceaf2a",
					"username": "hafizdwp"
				},
				{
					"id_user": "59f1b002bbe9e43288614286",
					"username": "viercas"
				}
			]
		},
		{
			"id_room": "room_59f1bc086e099140b4b9eeef",
			"name": null,
			"type": "group",
			"photo": null,
			"date": "2017-10-26T10:40:21.809Z",
			"user": [
				{
					"id_user": "59f1b90edc29de41d8ceaf2a",
					"username": "hafizdwp"
				},
				{
					"id_user": "59f1b063bbe9e4328861428d",
					"username": "themen"
				},
				{
					"id_user": "59f1b002bbe9e43288614286",
					"username": "viercas"
				}
			]
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Create Room Private

Request : 

* Link : POST {{ domain }}/api/v1/room

* Authorization : Bearer Token

|Field|Required|Value|
|-|-|-|
|room_type|yes|private|
|user|yes|string refers to `username` example : `themen` |

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Room Created !"
	},
	"data": {
		"id_room": "room_59f1ba17a8c0263d68187169",
		"name": null,
		"type": "private",
		"photo": null,
		"date": "2017-10-26T10:33:54.711Z",
		"user": [
			{
				"id_user": "59f1b90edc29de41d8ceaf2a",
				"username": "hafizdwp"
			},
			{
				"id_user": "59f1b002bbe9e43288614286",
				"username": "viercas"
			}
		]
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Room Already Created !"
	},
	"data": {
		"type": "private",
		"__v": 0,
		"id_room": "room_59d1f30c0a389f403c73932b",
		"photo": null,
		"date": "2017-10-02T08:04:12.595Z",
		"user": [
			{
				"username": "viercas",
				"email": "farhan.naufalghani@gmail.com",
				"__v": 0
			},
			{
				"username": "themen",
				"email": "themenkolk@gmail.com",
				"__v": 0
			}
		],
		"name": null
	}
}
```

### Create Room Group

Request : 

* Link : POST {{ domain }}/api/v1/room

* Authorization : Bearer Token

|Field|Required|Value|
|-|-|-|
|room_type|yes|group|
|user|yes|array refers to `username` example : `themen` |

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Room Created"
	},
	"data": {
		"type": "group",
		"__v": 0,
		"id_room": "room_59d1f39d0a389f403c73932e",
		"photo": null,
		"date": "2017-10-02T08:04:12.595Z",
		"user": [
			{
				"username": "viercas",
				"email": "farhan.naufalghani@gmail.com",
				"__v": 0
			},
			{
				"username": "vier",
				"email": "farhan.asd@gmail.com",
				"__v": 0
			},
			{
				"username": "themen",
				"email": "themenkolk@gmail.com",
				"__v": 0
			}
		],
		"name": null
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "type room group, Body User Required Array Type !"
}
```

## Firebase Cloud Messaging

### FCM Send

Request : 

* Link : POST {{ domain }}/api/v1/fcm

* Authorization : Bearer Token

|Field|Required|Value|
|-|-|-|
|token|yes|string|
|title|yes|string|
|message|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Room Created"
	},
	"data": {
		"type": "group",
		"__v": 0,
		"id_room": "room_59d1f39d0a389f403c73932e",
		"photo": null,
		"date": "2017-10-02T08:04:12.595Z",
		"user": [
			{
				"username": "viercas",
				"email": "farhan.naufalghani@gmail.com",
				"__v": 0
			},
			{
				"username": "vier",
				"email": "farhan.asd@gmail.com",
				"__v": 0
			},
			{
				"username": "themen",
				"email": "themenkolk@gmail.com",
				"__v": 0
			}
		],
		"name": null
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "type room group, Body User Required Array Type !"
}
```