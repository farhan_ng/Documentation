# Lead E-Commerce API

Hello there. Thanks for using Lead E-Commerce API. This documentation try to cover all Lead E-Commerce API.

## Structure

The Envelope :

```json
{
  "meta": {
    "code": 200,
    "message": "API call successful.",
  },
  "data": ...
}
```
Error Response : 

```json
{
    "meta": {
        "code": 500,
		"status": "Error",
    },
    "data" : ...
}
```


### The Envelope
Every response is contained by an envelope. That is, each response has a predictable set of keys with which you can expect to interact:

### META
The meta key is used to communicate extra information about the response to the developer. If all goes well, you'll only ever see a code key with value 200. However, sometimes things go wrong, and in that case you might see a response like:

### DATA

The data key is the meat of the response. It may be a list or dictionary, but either way this is where you'll find the data you requested.

<aside class="notice">With the exception is OAuth2 API, that will explained later in Authorize section.</aside>

## Datatype
```json
{
  "meta": {
    "code": 200,
    "message": "Success."
  },
  "data": {
    "id": "5639ce5e778b5fa40148bed3",
    "number": 2,
    "username": "smittyfirst",
    "name": "Smitty Werben Jager man Jensen",
    "alive": false,
    "date": "2015-11-19T09:47:46.493Z"
  }
}
```
* Id or such will always be a string, even if it's look like a number. 

* Response with number will return number (ex. `1`, `322`), not quoted number.

* Respectively, bool will return `true` or `false`.

* Date will be returned as ISO 8601 string. Example `December 25th, 2015` will be sent as `"2015-12-25T00:00:00Z"`.


## Header

Use `appication/json` as `Content-Type` header. Even without it this API would not break, it's still a good practice.

Do note every API need you to provide either `access_token` or Authorization Header. Use following id and secret to generate correct header.

|Basic Auth|Public|
|-|-|
|ID|dycode|
|Secret|dycode|

|Bearer Token|User Access Token|
|-|-|
|Value|{{ refers to `access_token` in Login API ex : `c29504439314c5b072c665db8f2e9e650cc199ea` }}|



## Authentication

### Register

Request : 


* Link : POST {{ domain }}/api/v1/register

* Authorization : Basic Auth


|Field|Required|Value|
|-|-|-|
|email|yes|string|
|username|yes|string|
|password|yes|string|
|repassword|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Successful Registered"
	},
	"data": "Link Email Verification has been sent to your Email, please check them"
}
```

Error Response : 

```
json
{
	"meta": {
		"code": 400,
		"message": "Email Already"
	},
	"data": "Email is already exist"
}
```



### Login
Request : 


* Link : POST {{ domain }}/api/v1/signin

* Authorization : Basic Auth

* Description : Login body Username by value `username` or `email` and body Password by `password` 

|Field|Required|Value|
|-|-|-|
|username|yes|string|
|password|yes|string|
  
Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Token Generated"
	},
	"data": {
		"user": {
			"email": "farhan.naufalghani@gmail.com",
			"username": "viercas",
			"__v": 0
		},
		"access_token": "f7af7f7f5ef56ae1818fcfd22010d1dc6e5ab3e6",
		"refresh_token": "e124bdd09b347777ad01eacbe754ae02ed64f6df",
		"accessTokenExpiresAt": "2017-09-19T10:26:29.826Z",
		"refreshTokenExpiresAt": "2017-10-03T09:26:29.826Z"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Email or Username Invalid"
	},
	"data": "Email or Username Not Found or Invalid"
}
```

### Refresh Login
Request : 

* Description : If you got Token expired you can got new Token by serving `refresh_token`

* Link : POST {{ domain }}/api/v1/signin

* Authorization : Basic Auth

|Field|Required|Value|
|-|-|-|
|grant_type|yes|refresh_token|
|refresh_token|yes|string refers to `refresh_token` in Login module ex : `e7e1d4e8690f4d9dd8722e4a8e907664d49c3069`|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Token Generated"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
			"__v": 0
		},
		"access_token": "d729f267b458ec40a49cdf5b45a9c2a78308f32d",
		"refresh_token": "0c11c8dcf14afc30192d35efc59928db17ceb2c7",
		"accessTokenExpiresAt": "2017-09-28T11:55:25.575Z",
		"refreshTokenExpiresAt": "2017-10-12T10:55:25.575Z"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "Refresh Token Invalid"
}
```

### Forgot Password

Request : 


* Link : POST {{ domain }}/api/v1/forgotPassword

* Authorization : Basic Auth



|Field|Required|Value|
|-|-|-|
|email|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "SUCCESS"
	},
	"data": "Link forgot Password has been sent to Your Email"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "Email not Found"
}
```

## User

### Profile
Request : 


* Link : GET {{ domain }}/api/v1/user

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Data found"
	},
	"data": {
		"user": {
			"username": "viercas",
			"email": "farhan.naufalghani@gmail.com",
			"__v": 0,
			"data": "59dc456c3c239803242aa8d0"
		},
		"userdata": {
			"__v": 0,
			"photo": {
				"date": "2017-10-10T03:59:44.253Z",
				"photo": "http://localhost:3000/img/59dc45b03c239803242aa8d5.png"
			},
			"status": "Single",
			"fullname": "Farhan Naufal"
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Update Profile

Request : 


* Link : POST {{ domain }}/api/v1/user

* Authorization : Bearer Token


|Field|Required|Value|
|-|-|-|
|fullname|no|string|
|status|no|string|
|photo|no|file|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Profile Updated"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "There is no Update!"
	},
	"data": "Use Body fullname, status, or photo to Update !"
}
```

### Change Password

Request : 

* Link : POST {{ domain }}/api/v1/password

* Authorization : Bearer Token



|Field|Required|Value|
|-|-|-|
|oldPassword|yes|string|
|newPassword|yes|string|
|reNewPassword|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Password has Changed"
	},
	"data": {
		"user": {
			"username": "farhan.naufalghani@gmail.com",
			"__v": 0
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Password not Match"
	},
	"data": "New Password and Retype New Password not Match"
}
```

### Search User
Request : 


* Link : GET {{ domain }}/api/v1/user/find/{{ refers to `username` example : `viercas` }}

* Authorization : Basic


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "User Found"
	},
	"data": {
		"email": "farhan.naufalghani@gmail.com",
		"username": "viercas",
		"__v": 0,
		"data": {
			"photo": null,
			"status": null,
			"fullname": null
		}
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "User Not Found"
}
```




## Product

### Get List Product
Request : 


* Link : GET {{ domain }}/api/v1/product?option={{ refres to `new` or `trendy`, if `null` may shows all product }}&offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Basic


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"id_product": "59cb2fe1a7ad812fe00349d6",
			"name": "Apple Watch 2",
			"color": "Grey",
			"quantity": 2,
			"price": 2999999,
			"photo": [
				{
					"_id": "59cb2fe1a7ad812fe00349d7",
					"date": "2017-09-27T04:58:09.255Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb2fe1a7ad812fe00349d7.jpg"
				}
			],
			"trendy": false,
			"hot": false,
			"likes": 0,
			"comments": 0
		},
		{
			"id_product": "59cb1b60a7ad812fe00349d3",
			"name": "Apple Pack Day",
			"color": "Gray, Black",
			"quantity": 4,
			"price": 2999999,
			"photo": [
				{
					"_id": "59cb1b60a7ad812fe00349d5",
					"date": "2017-09-27T03:30:40.763Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d5.jpg"
				},
				{
					"_id": "59cb1b60a7ad812fe00349d4",
					"date": "2017-09-27T03:30:40.742Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d4.jpg"
				}
			],
			"trendy": true,
			"hot": false,
			"likes": 2,
			"comments": 2
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "invalid_client"
	},
	"data": "Invalid client: client credentials are invalid"
}
```

### Get Hot Product
Request : 


* Link : GET {{ domain }}/api/v1/product/hot?option={{ refres to `new` or `trendy`, if `null` may shows all product }}&offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Basic


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"id_product": "59cb300ea7ad812fe00349d8",
			"name": "Apple Iphone 7",
			"color": "Red",
			"quantity": 2,
			"price": 11999999,
			"photo": [
				{
					"_id": "59cb300ea7ad812fe00349d9",
					"date": "2017-09-27T04:58:54.140Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb300ea7ad812fe00349d9.jpg"
				}
			],
			"trendy": true,
			"hot": true,
			"likes": 0,
			"comments": 0
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "invalid_client"
	},
	"data": "Invalid client: client credentials are invalid"
}
```

### Search Product
Request : 


* Link : GET {{ domain }}/api/v1/product/search/{{ refers to value that you may find example : `apple` }}?offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Basic


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"id_product": "59cb300ea7ad812fe00349d8",
			"name": "Apple Iphone 7",
			"color": "Red",
			"quantity": 2,
			"price": 11999999,
			"photo": [
				{
					"_id": "59cb300ea7ad812fe00349d9",
					"date": "2017-09-27T04:58:54.140Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb300ea7ad812fe00349d9.jpg"
				}
			],
			"trendy": true,
			"hot": true,
			"likes": 0,
			"comments": 3
		},
		{
			"id_product": "59cb2fe1a7ad812fe00349d6",
			"name": "Apple Watch 2",
			"color": "Grey",
			"quantity": 2,
			"price": 2999999,
			"photo": [
				{
					"_id": "59cb2fe1a7ad812fe00349d7",
					"date": "2017-09-27T04:58:09.255Z",
					"__v": 0,
					"photo": "http://localhost:3075/img/59cb2fe1a7ad812fe00349d7.jpg"
				}
			],
			"trendy": false,
			"hot": false,
			"likes": 0,
			"comments": 0
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "invalid_client"
	},
	"data": "Invalid client: client credentials are invalid"
}
```


### Get Product By Id

Request : 

* Link : GET {{ domain }}/api/v1/product/id/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}

* Authorization : Basic



Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": {
		"id_product": "59cb1b60a7ad812fe00349d3",
		"name": "Apple Pack Day",
		"color": "Gray, Black",
		"quantity": 4,
		"price": 2999999,
		"photo": [
			{
				"_id": "59cb1b60a7ad812fe00349d5",
				"date": "2017-09-27T03:30:40.763Z",
				"__v": 0,
				"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d5.jpg"
			},
			{
				"_id": "59cb1b60a7ad812fe00349d4",
				"date": "2017-09-27T03:30:40.742Z",
				"__v": 0,
				"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d4.jpg"
			}
		],
		"trendy": true,
		"hot": false,
		"likes": 2,
		"comments": 2
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Products not Found"
}
```


## Comment

### Get List Comment

Request : 


* Link : GET {{ domain }}/api/v1/product/comment/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}?offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
            "id_comment": "59ddd1237cd8582b8889b4c4",
			"id_product": "59cb1b60a7ad812fe00349d3",
			"user": "viercas",
			"value": "Barang Bagus",
			"date": "2017-09-27T07:05:44.917Z"
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "invalid_client"
	},
	"data": "Invalid client: client credentials are invalid"
}
```

### Input Comment

Request : 


* Link : POST {{ domain }}/api/v1/product/comment/{{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3`}}

* Authorization : Bearer Token



|Field|Required|Value|
|-|-|-|
|comment|yes|string|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Product Has Commented"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "Product not Found"
}
```

### Delete Comment

Request : 


* Link : DELETE {{ domain }}/api/v1/product/comment/{{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3`}}?id_comment={{ refres to `id_comment` example: `59cb1b60a7ad812fe00349d3`}}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "Comment Deleted"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Access Denied"
	},
	"data": "You not have Access this Action"
}
```

## Favorite

### Add or Remove Favorite / Like or Unliked Product


Request : 

* Link : POST {{ domain }}/api/v1/product/id/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}/like

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Success Liked Product"
}
```

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "You Unliked the Product !"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Products not Found"
}
```

### Get List User Liked Product

Request : 


* Link : GET {{ domain }}/api/v1/product/id/{{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3`}}/like?offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Basic Auth


Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": [
		{
			"user": {
				"username": "viercas",
				"fullname": "Farhan Naufal",
				"status": "Single",
				"photo": "http://localhost:3000/img/59c8863b540f2f4254f969c5.jpg"
			},
			"date": "2017-10-04T09:52:35.780Z"
		},
		{
			"user": {
				"username": "themen",
				"fullname": null,
				"status": null,
				"photo": null
			},
			"date": "2017-10-04T10:12:24.892Z"
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Product not Found"
}
```


### My Favorite

Request : 


* Link : GET {{ domain }}/api/v1/user/favorite?offset={{ offset example: 1 or 2 }}&limit={{ limit example: 1 or 2 }}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": [
		{
			"product": {
				"id_product": "59cb1b60a7ad812fe00349d3",
				"price": 2999999,
				"quantity": 4,
				"color": "Gray, Black",
				"name": "Apple Pack Day",
				"photo": [
					{
						"_id": "59cb1b60a7ad812fe00349d5",
						"date": "2017-09-27T03:30:40.763Z",
						"__v": 0,
						"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d5.jpg"
					},
					{
						"_id": "59cb1b60a7ad812fe00349d4",
						"date": "2017-09-27T03:30:40.742Z",
						"__v": 0,
						"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d4.jpg"
					}
				]
			},
			"dateLiked": "2017-10-10T04:32:25.637Z"
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Check My Favorite

Request : 

* Link : GET {{ domain }}/api/v1/user/favorite/check/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Success"
	},
	"data": {
		"id_product": "59cb1b60a7ad812fe00349d3",
		"name": "Apple Pack Day",
		"hasLiked": true
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Product not Found"
}
```

## Wishlist

### Add or Remove Wishlist

Request : 

* Link : POST {{ domain }}/api/v1/product/id/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}/wish

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Product Success Added to Wishlist"
}
```

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "Product Success Removed in Wishlist"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Product not Found"
}
```

### My Wishlist

Request : 


* Link : GET {{ domain }}/api/v1/user/wishlist?offset={{ refrers to `offset` example: 1 or 2 }}&limit={{ refrers to `limit` example: 1 or 2 }}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"product": {
				"id_product": "59cb1b60a7ad812fe00349d3",
				"price": 2999999,
				"quantity": 4,
				"color": "Gray, Black",
				"name": "Apple Pack Day",
				"photo": [
					{
						"_id": "59cb1b60a7ad812fe00349d5",
						"date": "2017-09-27T03:30:40.763Z",
						"__v": 0,
						"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d5.jpg"
					},
					{
						"_id": "59cb1b60a7ad812fe00349d4",
						"date": "2017-09-27T03:30:40.742Z",
						"__v": 0,
						"photo": "http://localhost:3075/img/59cb1b60a7ad812fe00349d4.jpg"
					}
				]
			},
			"dateAdd": "2017-10-10T04:09:51.674Z"
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Check My Wishlist

Request : 

* Link : GET {{ domain }}/api/v1/user/wishlist/check/{{ refers to `id_product` example : `59cb1b60a7ad812fe00349d3`}}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Success"
	},
	"data": {
		"id_product": "59cb1b60a7ad812fe00349d3",
		"name": "Apple Pack Day",
		"hasWish": true
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "404 Not Found"
	},
	"data": "Product not Found"
}
```

## Cart

### Add Cart

* Link : POST {{ domain }}/api/v1/user/cart

* Authorization : Bearer Token



|Field|Required|Value|
|-|-|-|
|id_product|yes|string  refres to `id_product` example: `59cb1b60a7ad812fe00349d3`|
|quantity|yes|number|

Success Response : 

```json
{
	"meta": {
		"code": 201,
		"message": "Success"
	},
	"data": "Product has been added !"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "Product Already Added!"
}
```

### My Cart

* Link : GET {{ domain }}/api/v1/user/cart

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": {
		"product": [
			{
				"product": {
					"id_product": "59cb2fe1a7ad812fe00349d6",
					"price": 2999999,
					"quantity": 2,
					"color": "Grey",
					"name": "Apple Watch 2",
					"photo": [
						"http://localhost:3075/img/59cb2fe1a7ad812fe00349d7.jpg"
					]
				},
				"myQuantity": 1,
				"date": "2017-10-10T07:41:31.954Z"
			}
		],
		"subtotal": 2999999,
		"taxes": 10,
		"total": 3299998.9
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": {
		"statusCode": 401,
		"status": 401,
		"code": 401,
		"message": "Invalid token: access token is invalid",
		"name": "invalid_token"
	}
}
```

### Update Cart

* Link : PUT {{ domain }}/api/v1/user/cart?id_product={{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3` }}

* Authorization : Bearer Token



|Field|Required|Value|
|-|-|-|
|quantity|yes|number|

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "Success Updated"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	},
	"data": "Sorry, product quantity only 2 left"
}
```

### Delete Cart

* Link : DELETE {{ domain }}/api/v1/user/cart?id_product={{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3` }}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": "Success Deleted"
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "Product not Found in My Cart !"
}
```

### Check My Cart

* Link : GET {{ domain }}/api/v1/user/cart/check?id_product={{ refres to `id_product` example: `59cb1b60a7ad812fe00349d3` }}

* Authorization : Bearer Token

Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": {
		"id_product": "59cb300ea7ad812fe00349d8",
		"name": "Apple Iphone 7",
		"hasCart": false
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 404,
		"message": "404 Not Found"
	},
	"data": "Product not found"
}
```

## Payment

### Get Payment Lead Bank

* Link : GET {{ domain }}/api/v1/pay

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": {
		"bank_name": "Paypal",
		"account_number": "123"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": "Invalid token: access token has expired"
}
```

### Add Payment

* Link : POST {{ domain }}/api/v1/pay

* Authorization : Bearer Token

* Description : Pay stuff from cart

|Field|Required|Value|
|-|-|-|
|bank_name|yes|string|
|account_number|yes|number|
|onBehalf|yes|string|
|address|yes|string|
|phone|yes|number|
|province|yes|string|
|city|yes|string|
|posCode|yes|number|


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success, Plese complete your payments"
	},
	"data": {
		"esitimate_time": 600000,
		"payment_id": "5a2c52f15797f34ca4fb39c5"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid"
	}
}
```

### Complete Payment

* Link : PUT {{ domain }}/api/v1/pay

* Authorization : Bearer Token

* Content-Type : Multipart

* Description : Complete Payment from Add Payment

|Field|Required|Value|
|-|-|-|
|photo|yes|file|


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Payment Has Uploaded!"
	}
}
```

Error Response : 

```json
{
	"meta": {
		"code": 400,
		"message": "Invalid Payment ID"
	}
}
```

### Payment History

* Link : GET {{ domain }}/api/v1/user/pay

* Authorization : Bearer Token


Success Response : 

```json
{
	"meta": {
		"code": 200,
		"message": "Success"
	},
	"data": [
		{
			"address": {
				"__v": 0,
				"posCode": 0,
				"city": null,
				"province": null,
				"phone": 0,
				"address": null,
				"name": null
			},
			"__v": 0,
			"date": "2017-12-09T21:14:51.021Z",
			"status": "payed",
			"total": 4176.7,
			"taxes": 10,
			"subtotal": 3797,
			"account_number": "",
			"bank_name": "",
			"products": [
				{
					"_id": "59fac0a82ce4dd22acd80807",
					"id_product": "59fabf72aec2ad1828c6c14f",
					"user": "59fabe7a12c3d515c02dafee",
					"__v": 0,
					"date": "2017-11-02T06:52:15.846Z",
					"status": true,
					"quantity": 1
				},
				{
					"_id": "59fac247399a510b58cd6145",
					"id_product": "59fac12baec2ad1828c6c151",
					"user": "59fabe7a12c3d515c02dafee",
					"__v": 0,
					"date": "2017-11-02T06:59:19.824Z",
					"status": true,
					"quantity": 2
				}
			]
		}
	]
}
```

Error Response : 

```json
{
	"meta": {
		"code": 401,
		"message": "Invalid Token"
	},
	"data": "Invalid token: access token is invalid"
}
```
